$(function(){
    $("#conversation-form").on("submit", function(e){
        e.preventDefault();
        $.post("../htbin/chatsend.py", {msg:$("#msg").val()},
            success:function(){
                $.getJSON("../htbin/chatget.py", function(data){
                    var $domFragment = $(document.createDocumentFragment());
                    $.each(data, function(index, value) {
                        $domFragment.append("<p>" + value.date + " à " + value.time + " <strong>Nom:</strong> " + value.user + "</p>" + "<p>" + value.msg + "</p>");
                    });
                    $("#conversation").html($domFragment);
                });
            },
            error:function(e){
                alert(e);
            }
        );
    }
});