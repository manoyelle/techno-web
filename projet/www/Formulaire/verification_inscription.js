//vérification finales uniquement
/*
var form = document.querySelector("form");

form.addEventListener("submit", function(e){
	var birthdate = form.elements.birthdate.value;
	var userpwd = form.elements.userpwd.value;
	var usermail = form.elements.usermail.value;

	if(birthdate != ""){
		var regexBdt = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		var validiteDate = "";
		var color = "";
		if(!regexBdt.test(birthdate)){
			validiteDate = "date invalide";
			color = "Red";
			e.preventDefault();	
		}
		else{
			var compDate = birthdate.split("/");
			var jj = Number(compDate[0]);
			var mm = Number(compDate[1])-1;
			var aaaa = Number(compDate[2]);
			var birthdateVerif = new Date(aaaa, mm, jj);
			if((birthdateVerif.getFullYear() > new Date().getFullYear()) || (aaaa != birthdateVerif.getFullYear()) || (mm != birthdateVerif.getMonth()) || (jj != birthdateVerif.getDate())){
				validiteDate = "date invalide";
				color = "Red";
				e.preventDefault();
			}
			else{}
		}
		document.getElementById("aideDate").textContent = validiteDate;
		document.getElementById("aideDate").style.color = color;
	}
	else{}

	if(userpwd != ""){
		var regexMdp = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)([\w]{8,})$/;
		var validiteMdp = "";
		if (!regexMdp.test(userpwd)) {
			validiteMdp = "Mot de passe invalide";
			color = "Red";
			e.preventDefault();
		}
		else{}
		document.getElementById("aideMdp").textContent = validiteMdp;
		document.getElementById("aideMdp").style.color = color;
	}
	else{}

	if(usermail != ""){
		var regexCourriel = /.+@.+\..+/;
		var validiteCourriel = "";
		if (!regexCourriel.test(usermail)) {
			validiteCourriel = "Adresse invalide";
			color = "Red";
			e.preventDefault();
		}
		else{}
		document.getElementById("aideCourriel").textContent = validiteCourriel;
		document.getElementById("aideCourriel").style.color = color;
	}
	else{}

});
*/
//vérification dynamique
        
function deactivateHelp() {
    var helps = document.querySelectorAll(".help"),
    helpsLength = helps.length;
    for (var i = 0 ; i < helpsLength ; i++) {
        helps[i].style.display = "none";
    }
}

function getHelp(elements) {
    while (elements = elements.nextSibling) {
        if (elements.className === "help") {
            return elements;
        }
    }
    return false;
}

var check = {};

check["lastname"] = function(){
	var name = document.getElementById("lastname"),
		helpStyle = getHelp(name).style;
	if(name.value.length >= 1){
		name.className = "correct";
        helpStyle.display = "none";
		return true;
	}
	else{
		name.className = "incorrect";
        helpStyle.display = "inline-block";
		return false;
	}
};

check["firstname"] = function(){
	var name = document.getElementById("firstname"),
		helpStyle = getHelp(name).style;
	if(name.value.length >= 1){
		name.className = "correct";
        helpStyle.display = "none";
		return true;
	}
	else{
		name.className = "incorrect";
        helpStyle.display = "inline-block";
		return false;
	}
};
 
check["birthdate"] = function(){
	var birthdate = document.getElementById("birthdate"),
		helpStyle = getHelp(birthdate).style;
	if(birthdate.value != ""){
		var regexBdt = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
		if(!regexBdt.test(birthdate.value)){
			birthdate.className = "incorrect";
        	helpStyle.display = "inline-block";
			return false	
		}
		else{
			var compDate = birthdate.value.split("/");
			var jj = Number(compDate[0]);
			var mm = Number(compDate[1])-1;
			var aaaa = Number(compDate[2]);
			var birthdateVerif = new Date(aaaa, mm, jj);
			if((birthdateVerif.getFullYear() > new Date().getFullYear()) || (aaaa != birthdateVerif.getFullYear()) || (mm != birthdateVerif.getMonth()) || (jj != birthdateVerif.getDate())){
				birthdate.className = "incorrect";
        		helpStyle.display = "inline-block";
				return false;
			}
			else{
				birthdate.className = "correct";
        		helpStyle.display = "none";
				return true;
			}
		}
	}
	else{
		birthdate.className = "correct";
        helpStyle.display = "none";
		return true;
	}
};

check["username"] = function(){
	var username = document.getElementById("username"),
		helpStyle = getHelp(username).style;
	if(username.value.length >= 6){
		username.className = "correct";
        helpStyle.display = "none";
		return true;
	}
	else{
		username.className = "incorrect";
        helpStyle.display = "inline-block";
		return false;
	}
};

check["userpwd"] = function(){
	var userpwd = document.getElementById("userpwd"),
		helpStyle = getHelp(userpwd).style;
	if(userpwd.value != ""){
		var regexMdp = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)([\w]{8,})$/;
		if (!regexMdp.test(userpwd.value) || userpwd.length < 8) {
			userpwd.className = "incorrect";
        	helpStyle.display = "inline-block";
			return false;
		}
		else{
			userpwd.className = "correct";
        	helpStyle.display = "none";
			return true;
		}
	}
	else{
		userpwd.className = "incorrect";
        helpStyle.display = "inline-block";
		return false;
	}
};

check["usermail"] = function(){
	var usermail = document.getElementById("usermail"),
		helpStyle = getHelp(usermail).style;
	if(usermail.value != ""){
		var regexCourriel = /.+@.+\..+/;
		if (!regexCourriel.test(usermail.value)) {
			usermail.className = "incorrect";
        	helpStyle.display = "inline-block";
			return false;
		}
		else{
			usermail.className = "correct";
        	helpStyle.display = "none";
			return true;
		}
	}
	else{
		usermail.className = "incorrect";
        helpStyle.display = "inline-block";
		return false;
	}
};

(function() { 
    var form = document.getElementById("form"),
       	inputs = document.getElementsByTagName("input");
       
    for (var i = 0 ; i < inputs.length ; i++) {
        if (inputs[i].type == "text" || inputs[i].type == "password") {       
            inputs[i].addEventListener("keyup", function(e) {
                check[e.target.id](e.target.id); 
            }, false);
        }
    }
    form.addEventListener("submit", function(e) {     
        var result = true;
        for (var i in check) {
            result = check[i](i) && result;
        }
        if (result) {
            //alert('Le formulaire est bien rempli.');
        }
        else{
        	e.preventDefault();
        }
    }, false);
    form.addEventListener("reset", function() {
        for (var i = 0 ; i < inputs.length ; i++) {
            if (inputs[i].type == "text" || inputs[i].type == "password") {
                inputs[i].className = "";
            }
        }
        deactivateHelp();
    }, false);
})();
deactivateHelp();