function sendFile(file){ 
  //initialisation xhr
  var xhr;
  if (window.XMLHttpRequest) {
    xhr = new XMLHttpRequest();
  }
  else if (window.ActiveObject) {
    xhr = new ActiveObject("Microsoft.XMLHTTP");
  }

  //requete asynchrone 
  xhr.onreadystatechange = function() { 
    if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200){
      document.getElementById("answer").innerHTML = "<span>" + xhr.responseText + "</span>"; //affichage
    }
    else{
    }
  }; 

  //ouverture et récupération du contenu du fichier
  xhr.open("POST", file, true);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send("username=" + document.getElementById("username").value +"&userpwd=" + document.getElementById("userpwd").value); //envoit
};

//récupération des événements qui déclanchent la requête
document.getElementById("connexion-form").addEventListener("submit", function(event){
  event.preventDefault();
  sendFile("../htbin/login.py");
});
